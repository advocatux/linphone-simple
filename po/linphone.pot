# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the linphone package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: linphone\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-22 01:06+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/SettingsPage.qml:15 ../qml/components/MainHeader.qml:30
#: ../qml/DialerPage.qml:50
msgid "Settings"
msgstr ""

#: ../qml/SettingsPage.qml:32
msgid "Logged in as:"
msgstr ""

#: ../qml/SettingsPage.qml:32 ../qml/SettingsPage.qml:65
msgid "Log in with Existing Account"
msgstr ""

#: ../qml/SettingsPage.qml:35 ../qml/components/ActiveAccount.qml:20
#: ../qml/Main.qml:101 ../qml/Main.qml:294
msgid "offline"
msgstr ""

#: ../qml/SettingsPage.qml:35
msgid "Connecting"
msgstr ""

#: ../qml/SettingsPage.qml:35
msgid "or create a Linphone SIP account"
msgstr ""

#: ../qml/SettingsPage.qml:44
msgid "Log out"
msgstr ""

#: ../qml/SettingsPage.qml:44 ../qml/WelcomePage.qml:74
msgid "Log in"
msgstr ""

#: ../qml/SettingsPage.qml:56
msgid "Create"
msgstr ""

#: ../qml/SettingsPage.qml:72
msgid "Stop Linphone when close"
msgstr ""

#: ../qml/SettingsPage.qml:75
msgid "Will not run in the background"
msgstr ""

#: ../qml/components/CommonDomains.qml:8
msgid "Add a known SIP domain:"
msgstr ""

#: ../qml/components/ContactIcon.qml:31
msgid "Contact"
msgstr ""

#: ../qml/components/MainHeader.qml:20
msgid "Info"
msgstr ""

#: ../qml/components/ListOfContacts.qml:38
msgid "Delete"
msgstr ""

#: ../qml/components/ListOfContacts.qml:52
msgid "Add"
msgstr ""

#: ../qml/components/DelFavContact.qml:13
#: ../qml/components/DelFavContact.qml:22
msgid "Delete Contact"
msgstr ""

#: ../qml/components/DelFavContact.qml:14
msgid "Delete favorite contact"
msgstr ""

#: ../qml/components/DelFavContact.qml:34
#: ../qml/components/AddFavContact.qml:104
msgid "Cancel"
msgstr ""

#: ../qml/components/AddFavContact.qml:41
msgid "Add Favorite Contact"
msgstr ""

#: ../qml/components/AddFavContact.qml:49
msgid "Contact <b>Name</b>"
msgstr ""

#: ../qml/components/AddFavContact.qml:61
msgid "Full <b>SIP Address</b>"
msgstr ""

#: ../qml/components/AddFavContact.qml:89
msgid "Add Contact"
msgstr ""

#: ../qml/components/AccountsHeader.qml:18
msgid "Information"
msgstr ""

#: ../qml/WelcomePage.qml:12
msgid "First Run"
msgstr ""

#: ../qml/WelcomePage.qml:58
msgid "Welcome to Linphone"
msgstr ""

#: ../qml/WelcomePage.qml:66
msgid "Do you have an existing SIP account? You can log in to use it."
msgstr ""

#: ../qml/WelcomePage.qml:82
msgid ""
"Do you want to create a new SIP account with Linphone? Tap to open Linphone "
"website and sign up."
msgstr ""

#: ../qml/WelcomePage.qml:89
msgid "Sign up"
msgstr ""

#: ../qml/LinphoneAccount.qml:16
msgid "SIP Accounts"
msgstr ""

#: ../qml/LinphoneAccount.qml:59
msgid "<b>Username</b> of the SIP account"
msgstr ""

#: ../qml/LinphoneAccount.qml:89
msgid ""
"Provider <b>Domain</b>. Usually, what it goes after @ of your SIP address"
msgstr ""

#: ../qml/LinphoneAccount.qml:116
msgid "<b>Password</b> of the SIP account"
msgstr ""

#: ../qml/LinphoneAccount.qml:138
msgid "Login"
msgstr ""

#: ../qml/Keypad.qml:51
msgid "1"
msgstr ""

#: ../qml/Keypad.qml:69
msgid "2"
msgstr ""

#: ../qml/Keypad.qml:86
msgid "3"
msgstr ""

#: ../qml/Keypad.qml:103
msgid "4"
msgstr ""

#: ../qml/Keypad.qml:120
msgid "5"
msgstr ""

#: ../qml/Keypad.qml:137
msgid "6"
msgstr ""

#: ../qml/Keypad.qml:154
msgid "7"
msgstr ""

#: ../qml/Keypad.qml:170
msgid "8"
msgstr ""

#: ../qml/Keypad.qml:187
msgid "9"
msgstr ""

#: ../qml/Keypad.qml:206
msgid "*"
msgstr ""

#: ../qml/Keypad.qml:223
msgid "0"
msgstr ""

#: ../qml/Keypad.qml:224
msgid "+"
msgstr ""

#: ../qml/Keypad.qml:244
msgid "#"
msgstr ""

#: ../qml/Main.qml:175
msgid "SIP address to call"
msgstr ""

#: ../qml/Main.qml:241
msgid "Incoming Call from URL"
msgstr ""

#: ../qml/Main.qml:358
msgid "Dev"
msgstr ""

#: ../qml/Main.qml:367
msgid "Development"
msgstr ""

#: ../qml/Main.qml:379
msgid "Send a command to Linphone"
msgstr ""

#: ../qml/DialerPage.qml:56
msgid "Contacts"
msgstr ""

#: ../qml/DialerPage.qml:62
msgid "Messages"
msgstr ""

#: ../qml/DialerPage.qml:68
msgid "Dialer"
msgstr ""

#: ../qml/DialerPage.qml:86
msgid "Close"
msgstr ""

#: ../qml/DialerPage.qml:117
msgid "Phone"
msgstr ""

#: ../qml/DialerPage.qml:228
msgid "Here goes the account name"
msgstr ""

#: ../qml/DialerPage.qml:306
msgid "Enter a number"
msgstr ""

#: ../qml/DialerPage.qml:479
msgid "Calling"
msgstr ""

#: ../qml/IncomingCall.qml:35
msgid "Incoming Call"
msgstr ""

#: ../qml/IncomingCall.qml:55 ../qml/OutgoingCall.qml:46
msgid "Duration: "
msgstr ""

#: ../qml/IncomingCall.qml:55 ../qml/OutgoingCall.qml:46
msgid " seconds"
msgstr ""

#: ../qml/OutgoingCall.qml:32
msgid "In Call"
msgstr ""

#: ../qml/OutgoingCall.qml:32
msgid "Calling:"
msgstr ""

#: linphone.desktop.in.h:1
msgid "Linphone"
msgstr ""
