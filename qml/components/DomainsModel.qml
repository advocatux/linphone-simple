import QtQuick 2.4
import Ubuntu.Components 1.3

ListModel {
    id: domainsModel

    ListElement { name: "callcentric.com" }

    ListElement { name: "sip.ippi.com" }

    ListElement { name: "sip.linphone.org" }

}
